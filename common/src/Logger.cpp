#include "Logger.hpp"
#include <iostream>
#include <boost/log/common.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/attributes/timer.hpp>
#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/support/date_time.hpp>

namespace logger {



namespace logging = boost::log;
namespace sinks = boost::log::sinks;
namespace attrs = boost::log::attributes;
namespace src = boost::log::sources;
namespace expr = boost::log::expressions;
namespace keywords = boost::log::keywords;
using boost::shared_ptr;

// The formatting logic for the severity level
template< typename CharT, typename TraitsT >
inline std::basic_ostream< CharT, TraitsT >& operator<< (
    std::basic_ostream< CharT, TraitsT >& strm, severity_level lvl)
{
    static const char* const str[] =
    {
        "info",
        "normal",
        "warning",
        "error"
    };
    if (static_cast< std::size_t >(lvl) < (sizeof(str) / sizeof(*str)))
        strm << str[lvl];
    else
        strm << static_cast< int >(lvl);
    return strm;
}


void initLogger(std::filesystem::path logFile) {

    logging::add_console_log(std::clog, keywords::format = "%TimeStamp%: %Message%");
    logging::add_file_log
    (
        logFile,
        //keywords::filter = expr::attr< severity_level >("Severity") >= warning,
        keywords::format = expr::stream
            << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d, %H:%M:%S.%f")
            //<< " [" << expr::format_date_time< attrs::timer::value_type >("Uptime", "%O:%M:%S")
            << "] [" << expr::format_named_scope("Scope", keywords::format = "%n (%f:%l)")
            << "] <" << expr::attr< severity_level >("Severity")
            << "> " << expr::message
    );
    logging::add_common_attributes();
    logging::core::get()->add_thread_attribute("Scope", attrs::named_scope());
    BOOST_LOG_FUNCTION();

    src::logger lg;
    BOOST_LOG(lg) << "Logger initialized";
}


boost::log::sources::severity_logger<severity_level> & Warning() {
    static src::severity_logger<severity_level> logger(severity_level::warning);
    return logger;
}


boost::log::sources::severity_logger<severity_level> & Normal() {
    static src::severity_logger<severity_level> logger(severity_level::normal);
    return logger;
}


boost::log::sources::severity_logger<severity_level> & Error() {
    static src::severity_logger<severity_level> logger(severity_level::error);
    return logger;
}


boost::log::sources::severity_logger<severity_level> & Info() {
    static src::severity_logger<severity_level> logger(severity_level::info);
    return logger;
}

} //namespace logger