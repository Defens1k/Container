#include "Common.hpp"
#include "Logger.hpp"
#include <thread>
#include <chrono>

namespace common {

constexpr const size_t kPortNumber = 3333;
constexpr const size_t kDefaultThreads = 32;

size_t portNumber() {
    return kPortNumber;
}

size_t threadsCount() {
    size_t count = std::thread::hardware_concurrency() * 2;

    if (count == 0) {
        count = kDefaultThreads;
    }
    BOOST_LOG(logger::Info()) << "Threads count: " << std::to_string(count) << std::endl;
    return count;
}


} // namespace common