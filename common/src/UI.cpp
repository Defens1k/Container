#include "UI.hpp"
#include <SFML/Graphics.hpp>

namespace common {


void UI::wait(const std::string & message) {
 
    sf::Window window(
        sf::VideoMode(640, 480),
        message);
    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event))
            if (event.type ==
            sf::Event::Closed)
                window.close();
    }
}

} //namespace common