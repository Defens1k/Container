#pragma once
#include <string>

namespace common {

class UI {
public:
    void wait(const std::string & message);
};

} //namespace common