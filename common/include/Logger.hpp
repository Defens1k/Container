#pragma once

#include <boost/log/common.hpp>
#include <boost/log/sources/logger.hpp>
#include <memory>
#include <filesystem>

namespace logger {
enum severity_level {
    info,
    normal,
    warning,
    error
};

void initLogger(std::filesystem::path logFile);

boost::log::sources::severity_logger<severity_level> & Error();
boost::log::sources::severity_logger<severity_level> & Warning();
boost::log::sources::severity_logger<severity_level> & Normal();
boost::log::sources::severity_logger<severity_level> & Info();

} //namespce logger