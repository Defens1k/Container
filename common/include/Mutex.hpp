#pragma once
#include <semaphore>

namespace common {

class SharedMutex : private std::binary_semaphore {
public:
    SharedMutex(): std::binary_semaphore(1) {}
    void lock() {
        acquire();
    }
    void unlock() {
        release();
    }
};



} //namespce common