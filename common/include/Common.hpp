#pragma once
#include <cstddef>
#include <string>

namespace common {

constexpr const int N = 3;

size_t portNumber();

size_t threadsCount();

} // namespace common
