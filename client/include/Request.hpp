#pragma once
#include "Mutex.hpp"
#include <boost/asio.hpp>

namespace client {
using namespace boost;

using Callback = std::function<void(unsigned int, 
                            const std::string &, 
                            const boost::system::error_code &)>;

struct Request {
    Request(
        std::shared_ptr<std::lock_guard<common::SharedMutex>> requestLock,
        unsigned int requestId,
        std::string request,
        Callback callback
        ) : requestLock(requestLock),
            requestId(requestId),
            request(std::move(request)),
            callback(callback) {};
    std::shared_ptr<std::lock_guard<common::SharedMutex>> requestLock;
    unsigned int requestId;
    std::string request;
    std::string response;
    boost::asio::streambuf responseBuf;
    boost::system::error_code ec;
    bool wasCanceled = false;
    Callback callback;
};

} //namespace client