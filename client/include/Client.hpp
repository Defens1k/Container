#pragma once
#include "Request.hpp"

#include <boost/asio.hpp>
#include <boost/noncopyable.hpp>

#include <thread>
#include <mutex>
#include <memory>
#include <list>


namespace client {
class AsyncTCPClient : public boost::noncopyable {
public:
    AsyncTCPClient(unsigned char numOfThreads = 1);
    
    void connect(
        const std::string &rawIpAddress,
        unsigned short portNum
    );

    void request(std::shared_ptr<Request> request);

    void cancelRequest(unsigned int requeestId);

    void close();
private:
    void onRequestComplete(std::shared_ptr<Request> request);

    std::function<void(const system::error_code &ec, std::size_t bytes_transferred)> afterWrite(std::shared_ptr<Request> request);
    std::function<void(const system::error_code &ec, std::size_t bytes_transferred)> afterRead(std::shared_ptr<Request> request);

private:
    boost::asio::io_service _ios;
    std::unique_ptr<boost::asio::io_service::work> _work;
    std::unique_ptr<std::thread> _thread;
    std::unique_ptr<boost::asio::ip::tcp::socket> _sock;
    std::unique_ptr<boost::asio::ip::tcp::endpoint> _ep;
};


} //namespace client