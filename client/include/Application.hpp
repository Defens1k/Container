#pragma once
#include "Client.hpp"
#include "Mutex.hpp"
#include <random>

namespace client {


class Application {
public:
    Application(const std::string & ip, size_t port);
    ~Application();
    [[nodiscard]] bool start();
private:
    bool _stop = false;
    size_t _port = 0;
    std::string _ip;
    common::SharedMutex _lock;
    Callback _handler;
    std::mt19937 _gen;
    std::uniform_int_distribution<> _distrib;
    std::shared_ptr<std::thread> _loop;
};


}; //namespace client