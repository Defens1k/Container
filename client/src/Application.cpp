#include "Application.hpp"
#include "Logger.hpp"

namespace client {
Application::Application(const std::string & ip, size_t port) : _ip(ip), _port(port) {
    _handler = [](unsigned int requeestId,
                const std::string &response,
                const system::error_code &ec) {
        if (ec.value() == 0) {
            BOOST_LOG(logger::Normal()) << "Request #" << requeestId
                    << " has completed. Response: "
                    << response;
        } else if (ec == asio::error::operation_aborted) {
            BOOST_LOG(logger::Normal()) << "Request #" << requeestId
                    << " has been cancelled by the user.";
            BOOST_LOG(logger::Info()) << "Sleep for 1 sec";
        } else {
            BOOST_LOG(logger::Error()) << "Request #" << requeestId
                    << " failed! Error code = " << ec.value()
                    << ". Error message = " << ec.message();
            BOOST_LOG(logger::Info()) << "Sleep for 1 sec";
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        return;
    };

    std::random_device rd;
    _gen = std::mt19937(rd()); 
    _distrib = std::uniform_int_distribution<>(0, 1023);
}

Application::~Application() {
    _stop = true;
    _loop->join();
}

bool Application::start() {

    auto client = std::make_shared<AsyncTCPClient>();
    try {
        client->connect(_ip, _port);
    } catch (const system::system_error & e) {
        BOOST_LOG(logger::Error()) << "Error while connecting to server. Code = "
            << e.code() << ". Message: " << e.what();
        return false;
    } catch (...) {
        BOOST_LOG(logger::Error()) << "Undefined error while connecting to server";
        return false;
    }

    auto loop = [this, client]() {


        size_t id = 0;
        while(1) {
            if (_stop) {
                break;
            }
            auto n = _distrib(_gen);
            id++;

            auto lock = std::make_shared<std::lock_guard<common::SharedMutex>>(_lock);
            std::string requestData = std::to_string(n) + "\n";
            auto request = std::make_shared<Request>(std::move(lock), id, std::move(requestData), _handler);

            try{
                client->request(std::move(request));
            } catch (system::system_error &e) {
                BOOST_LOG(logger::Error()) << "Error code = " << e.code()
                        << ". Message: " << e.what();
                continue;
            } catch (...) {
                BOOST_LOG(logger::Error()) << "Undefined error while client request";
                continue;
            }
        }
        BOOST_LOG(logger::Info()) << "Client close";
        client->close();
        BOOST_LOG(logger::Info()) << "Client closed";
    };
    _loop = std::make_shared<std::thread>(loop);
    return true;
}
} //namespace client