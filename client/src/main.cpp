#include <boost/program_options.hpp>
#include <iostream>

#include "Common.hpp"
#include "Application.hpp"
#include "UI.hpp"
#include "Logger.hpp"
namespace po = boost::program_options;

int main(int argc, char ** argv) {
    logger::initLogger("client_log.txt");
    std::string ip;
    size_t port;
    // Configure options here
    po::options_description desc ("Allowed options");
    std::string portDescription = std::string("Server port,  default value: ") + std::to_string(common::portNumber());
    desc.add_options ()
        ("help,h", "print usage message")
        ("ip,i", po::value(&ip), "Server IP address")
        ("port ,p", po::value<size_t>(&port)->default_value(common::portNumber()), portDescription.c_str());
    // Parse command line arguments
    po::variables_map vm;
    po::store (po::command_line_parser (argc, argv).options (desc).run (), vm);
    po::notify (vm);
    // Check if there are enough args or if --help is given
    if (vm.count ("help") || !vm.count ("ip")) {
        std::cerr << desc << "\n";
        return 1;
    }

    client::Application application(ip, port);
    auto started = application.start();
    if (started == false) {
        return 1;
    }

    common::UI ui;
    ui.wait("Client running. Close window to stop");
};