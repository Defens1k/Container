#include "Client.hpp"
#include "Logger.hpp"
#include <boost/asio.hpp>
#include <memory>

namespace client {
using namespace boost;

AsyncTCPClient::AsyncTCPClient(unsigned char numOfThreads) {

    _work.reset(new boost::asio::io_service::work(_ios));

    _thread = std::make_unique<std::thread>([this](){ _ios.run(); });
}



std::function<void(const system::error_code &ec, std::size_t bytes_transferred)> AsyncTCPClient::afterRead(std::shared_ptr<Request> request) {
    auto func = [this, request](const boost::system::error_code &ec,
                                            std::size_t bytes_transferred) {
        if (ec.value() != 0) {
            request->ec = ec;
        } else {
            std::istream strm(&request->responseBuf);
            std::getline(strm, request->response);
        }

        onRequestComplete(request);
    };
    return func;
}

std::function<void(const system::error_code &ec, std::size_t bytes_transferred)> AsyncTCPClient::afterWrite(std::shared_ptr<Request> request) {
    auto func = [this, request](const boost::system::error_code &ec,
                    std::size_t bytes_transferred) {
        if (ec.value() != 0) {
            request->ec = ec;
            onRequestComplete(request);
            return;
        }

        if (request->wasCanceled) {
            onRequestComplete(request);
            return;
        }

        auto readFunc = afterRead(request);
        asio::async_read_until(*_sock,
                                request->responseBuf,
                                '\n',
                                readFunc);
    };
    return func;
}


void AsyncTCPClient::connect(
    const std::string &rawIpAddress,
    unsigned short portNum) {

    _sock = std::make_unique<boost::asio::ip::tcp::socket>(_ios);
    _ep = std::make_unique<boost::asio::ip::tcp::endpoint>(
            boost::asio::ip::address::from_string(rawIpAddress),
            portNum);

    _sock->open(_ep->protocol());

    _sock->connect(*_ep);
}


void AsyncTCPClient::request(std::shared_ptr<Request> request) {
    auto writeFunc = afterWrite(request);

    asio::async_write(*_sock,
                    asio::buffer(request->request), 
                    writeFunc);
}


void AsyncTCPClient::close() {

    boost::system::error_code ignoredEc;
    _sock->shutdown( asio::ip::tcp::socket::shutdown_both, ignoredEc);
    _work.reset(NULL);

    _thread->join();
}

void AsyncTCPClient::onRequestComplete(std::shared_ptr<Request> request) {

    boost::system::error_code ec;

    if (request->ec.value() == 0 && request->wasCanceled) {
        ec = asio::error::operation_aborted;
    } else {
        ec = request->ec;
    }

    request->callback(request->requestId, request->response, ec);
};


} //namespace client