project(client)
cmake_minimum_required(VERSION 3.5)


set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR})

set(CMAKE_EXPORT_COMPILE_COMMANDS "ON")
set(Boost_USE_STATIC_LIBS ON)
set(CMAKE_CURRENT_SOURCE_DIR src)
set(CLIENT_SOURCES
   ${CMAKE_CURRENT_SOURCE_DIR}/Client.cpp
   ${CMAKE_CURRENT_SOURCE_DIR}/Application.cpp
)
find_package(Boost REQUIRED COMPONENTS thread system chrono program_options)

add_library(client STATIC ${CLIENT_SOURCES} )
set_target_properties(client PROPERTIES LINKER_LANGUAGE CXX)
target_include_directories(client PUBLIC include)

target_link_libraries(client 
   common 
   Boost::thread 
   Boost::system 
   Boost::chrono 
   Boost::program_options
)


add_executable(client_app 
   ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp
)

target_link_libraries(client_app client) 
