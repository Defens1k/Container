#pragma once
#include <mutex>
#include <memory>
#include <thread>
#include <chrono>
#include <vector>
#include <filesystem>
#include <condition_variable>
#include <atomic>

#include "Mutex.hpp"
#include "Dumper.hpp"

class Container {
public:
    Container(std::shared_ptr<Dumper> dumper, std::chrono::duration<int> dumpPeriod);
    ~Container();
    size_t store(size_t value);
    void forceDump();
private:
    void dump();
    void syncReplication();

    std::shared_ptr<Dumper> _dumper;
    size_t _sum = 0;
    std::atomic<bool> _stop = false;
    std::vector<size_t> _master;
    std::vector<size_t> _slave;
    std::shared_ptr<std::thread> _dumpLoop;
    std::condition_variable_any _dumpWait;
    common::SharedMutex _waitLock;
    std::mutex _lock;
    std::mutex _slaveLock;
    std::chrono::duration<int> _dumpPeriod;
};