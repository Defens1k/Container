#pragma once

#include "Container.hpp"
#include <boost/asio.hpp>

class Acceptor {
public:
    Acceptor(boost::asio::io_service &ios, unsigned short port_num, std::shared_ptr<Container> container) :
        _ios(ios),
        _container(std::move(container)),
        _acceptor(_ios,
            boost::asio::ip::tcp::endpoint(
            boost::asio::ip::address_v4::any(),
            port_num)),
        _isStopped(false) {};

    void Start();
    void Stop();
private:
    void InitAccept();
    void onAccept(const boost::system::error_code &ec,
                  std::shared_ptr<boost::asio::ip::tcp::socket> sock);

private:
    boost::asio::io_service &_ios;
    boost::asio::ip::tcp::acceptor _acceptor;
    std::atomic<bool> _isStopped;
    std::shared_ptr<Container> _container;
};
