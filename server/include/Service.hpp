#pragma once
#include <boost/asio.hpp>
#include "Container.hpp"

class Service {
public:
    Service(std::shared_ptr<boost::asio::ip::tcp::socket> sock,
            std::shared_ptr<Container> container) : 
                    _sock(std::move(sock)) ,
                    _container(std::move(container)){};

    void StartHandling();
private:
    void onRequestReceived(const boost::system::error_code &ec,
                           std::size_t bytes_transferred);
    void onResponseSent(const boost::system::error_code &ec,
                        std::size_t bytes_transferred);
    void onFinish(bool exit = true) {
        if (exit) {
            delete this;
        } else {
            StartHandling();
        }
    }

    std::string ProcessRequest(boost::asio::streambuf &request);
private:
    std::shared_ptr<boost::asio::ip::tcp::socket> _sock;
    std::shared_ptr<Container> _container;
    std::string _response;
    boost::asio::streambuf _request;
};
