#pragma once

#include "Acceptor.hpp"
#include "Container.hpp"
#include <boost/asio.hpp>


class Server {
public:
    Server(std::filesystem::path dumpsDir);

    void start(unsigned short port_num,
               unsigned int thread_pool_size);

    void stop();
private:
    boost::asio::io_service _ios;
    std::unique_ptr<boost::asio::io_service::work> _work;
    std::unique_ptr<Acceptor> _acc;
    std::vector<std::unique_ptr<std::thread>> _threadPool;
    std::shared_ptr<Container> _container;
};

