#pragma once
#include <vector>
#include <string>
#include <filesystem>

class Dumper {
public:
    virtual void dump(const std::vector<size_t> & data) = 0;
};

class BinaryFileDumper : public Dumper {
public:
    BinaryFileDumper(std::filesystem::path dumpsDir);
    
    virtual void dump(const std::vector<size_t> & data) override;
private:
    std::filesystem::path::string_type dumpName();

    size_t _dumpNumber = 1;
    std::filesystem::path _dumpsDir;
};