#include "Common.hpp"
#include "Application.hpp"
#include "Logger.hpp"
#include "Server.hpp"
#include <gtest/gtest.h>



TEST(Server, long_test) {

    logger::initLogger("long_test_log.txt");
    
    auto dumpsDir = std::filesystem::current_path();
    dumpsDir.append("test_long_dumps");
    Server srv(dumpsDir);

    srv.start(common::portNumber(), common::threadsCount());


    std::vector<std::shared_ptr<client::Application>> applications;
    for (auto i = 0; i < 100; i++) {
        applications.push_back(std::make_shared<client::Application>("127.0.0.1", common::portNumber()));
    }
    for (auto & app : applications) {
        auto value = app->start();
        ASSERT_EQ(value, true);
    }
    std::this_thread::sleep_for(std::chrono::minutes(20));
    
    ASSERT_NO_THROW(applications.clear());// delete clients first

    ASSERT_NO_THROW(srv.stop());

    BOOST_LOG(logger::Info()) << "exit ";
}