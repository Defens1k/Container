#include <gtest/gtest.h>
#include <memory>
#include "Container.hpp"
#include "Dumper.hpp"

class TestDumper : public Dumper{
public :
    virtual void dump(const std::vector<size_t> & data) override {
        _data.push_back(data);
    };
    const std::vector<std::vector<size_t>> & data() {
        return _data;
    };
private:
    std::vector<std::vector<size_t>> _data;
};


TEST(Container, SimplTest) {
    auto dumper = std::make_shared<TestDumper>();
    auto container = std::make_shared<Container>(dumper, std::chrono::seconds(9999));
    ASSERT_EQ(std::vector<std::vector<size_t>>(), dumper->data());

    std::vector<std::vector<size_t> > data{ { 1, 2, 100 },
                                    { 23, 21, 111112 },
                                    {},  {0, 1323, 1, 1, 1, 1, 1323}};

    std::vector<std::vector<size_t> > expected{ { 1, 2, 100 }, 
                { 1, 2, 100, 23, 21, 111112 }, 
                { 1, 2, 100, 23, 21, 111112 },
                { 1, 2, 100, 23, 21, 111112, 0, 1323, 1, 1, 1, 1, 1323 } };

    for (auto& vec: data) {
        for (auto value: vec) {
            container->store(value);
        }
        container->forceDump();
    }
    ASSERT_EQ(expected, dumper->data());


}

TEST(Container, AutoTest) {
    auto dumper = std::make_shared<TestDumper>();
    auto container = std::make_shared<Container>(dumper, std::chrono::seconds(1));
    ASSERT_EQ(std::vector<std::vector<size_t>>(), dumper->data());

    std::vector<std::vector<size_t> > data{ { 1, 2, 100 },
                                    { 23, 21, 111112 },
                                    {},  {0, 1323, 1, 1, 1, 1, 1323}};

    std::vector<std::vector<size_t> > expected{ { 1, 2, 100 }, 
                { 1, 2, 100, 23, 21, 111112 }, 
                { 1, 2, 100, 23, 21, 111112 },
                { 1, 2, 100, 23, 21, 111112, 0, 1323, 1, 1, 1, 1, 1323 } };


    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    for (auto& vec: data) {
        for (auto value: vec) {
            container->store(value);
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    ASSERT_EQ(expected, dumper->data());


}



TEST(Container, ThreadTest) {
    auto dumper = std::make_shared<TestDumper>();
    auto container = std::make_shared<Container>(dumper, std::chrono::seconds(9999));
    ASSERT_EQ(std::vector<std::vector<size_t>>(), dumper->data());

    size_t threads = 1000;
    size_t iterations = 10000;

    std::vector<std::shared_ptr<std::thread>> workers;

    for (size_t i = 0; i < threads; i++) {
        auto func = [iterations, container]() {
            for (size_t i = 0; i < iterations; i++) {
                container->store(1);
            }
        };
        auto thr = std::make_shared<std::thread>(func);
        workers.push_back(thr);
    }
    for (auto & thr : workers) {
        thr->join();
    }
    container->forceDump();
    ASSERT_EQ(threads * iterations, dumper->data()[0].size());


}