#include "Acceptor.hpp"
#include "Service.hpp"
#include "Logger.hpp"

using namespace boost;
void Acceptor::Start() {
    _acceptor.listen();
    InitAccept();
}


void Acceptor::Stop() {
    _isStopped.store(true);
}


void Acceptor::InitAccept() {
    std::shared_ptr<asio::ip::tcp::socket> sock(new asio::ip::tcp::socket(_ios));

    _acceptor.async_accept(*sock.get(),
                            [this, sock](
                                const boost::system::error_code &error) {
                                onAccept(error, sock);
                            });
}


void Acceptor::onAccept(const boost::system::error_code &ec,
                std::shared_ptr<asio::ip::tcp::socket> sock) {
    if (ec.value() == 0) {
        (new Service(sock, _container))->StartHandling();
    } else {
        BOOST_LOG(logger::Error()) << "Error on accept. Code = "
                    << ec.value()
                    << ". Message: " << ec.message();
    }

    if (!_isStopped.load()) {
        InitAccept();
    } else {
        _acceptor.close();
    }
}