#include "Container.hpp"
#include "Logger.hpp"
#include <sstream>
#include <iomanip>
#include <fstream>
#include <filesystem>
#include <string_view>


Container::Container(std::shared_ptr<Dumper> dumper, std::chrono::duration<int> dumpPeriod) {
    _dumper = std::move(dumper);
    _dumpPeriod = dumpPeriod;

    BOOST_LOG(logger::Info()) << "Creating container. Container will dumping every " 
        << dumpPeriod.count() << " seconds";
    auto loop = [this]() {
        {
            std::unique_lock wait(_waitLock);
            _dumpWait.wait_for(wait, _dumpPeriod, [this](){return _stop.load();});
        }
        while (1) {
            if (_stop) {
                return;
            }
            auto start = std::chrono::system_clock::now();

            forceDump();
            if (_stop) {
                return;
            }
            auto end = std::chrono::system_clock::now();
            auto diff = end - start;

            BOOST_LOG(logger::Info()) << "Dump completed. Dump duration = "
                    << std::chrono::duration_cast<std::chrono::milliseconds>(diff).count()
                    << " milliseconds";

            if (diff <= _dumpPeriod) {
                std::chrono::duration<double> sleepDuration(_dumpPeriod - diff);
                BOOST_LOG(logger::Info()) << "Dump thread sleep for " 
                    << std::chrono::duration_cast<std::chrono::milliseconds>(sleepDuration).count()
                    << " milliseconds";
                std::unique_lock wait(_waitLock);
                _dumpWait.wait_for(wait, sleepDuration, [this](){return _stop.load();});

            } else {
                BOOST_LOG(logger::Warning()) << "Warning, dump duration more then N";
            }

        }
    };
    _dumpLoop = std::make_shared<std::thread>(loop);
}


Container::~Container() {
    BOOST_LOG(logger::Info()) << "Container desctructor";
    _stop = true;
    _dumpWait.notify_all();
    _dumpLoop->join();
}


void Container::forceDump() {

    std::lock_guard slaveLock(_slaveLock);
    {
        std::lock_guard lock(_lock);
        syncReplication();
    }
    try {

        BOOST_LOG(logger::Info()) << "Start dumping container";
        _dumper->dump(_slave);
    } catch (const std::exception & e) {
        BOOST_LOG(logger::Error()) << "Error while dumping container: " << e.what();
    } catch (...) {
        BOOST_LOG(logger::Error()) << "Error while dumping container";
    }
    {
        std::lock_guard lock(_lock);
        syncReplication();
    }
}

size_t Container::store(size_t value) {
    std::lock_guard lock(_lock);
    _master.push_back(value);
    _sum += value * value;
    auto result = _sum / _master.size();
    auto slaveLocked = _slaveLock.try_lock();
    if (slaveLocked) {
        syncReplication();
        _slaveLock.unlock();
    }


    return result;
}

void Container::syncReplication() {
    auto diff = _master.size() - _slave.size();
    if (diff <= 0) {
        return;
    }
    auto startDiff = _master.begin();
    startDiff += _slave.size();
    std::copy(startDiff, _master.end(), std::back_inserter(_slave));
}