#include "Service.hpp"
#include "Logger.hpp"

using namespace boost;

void Service::StartHandling() {
    auto readFunc = [this](const boost::system::error_code &ec,
                            std::size_t bytes_transferred) {
        onRequestReceived(ec, bytes_transferred);
    };
    asio::async_read_until(*_sock.get(),
                            _request,
                            '\n',
                            readFunc);
}


void Service::onRequestReceived(const boost::system::error_code &ec,
                        std::size_t bytes_transferred) {
    if (ec.value() != 0) {
        BOOST_LOG(logger::Error()) << "Error on request received. Code = "
                    << ec.value()
                    << ". Message: " << ec.message();
        onFinish();
        return;
    }

    _response = ProcessRequest(_request);

    auto writeFunc = [this](const boost::system::error_code &ec,
                            std::size_t bytes_transferred) {
        onResponseSent(ec, bytes_transferred);
    };
    asio::async_write(*_sock.get(),
                        asio::buffer(_response),
                        writeFunc);
}



void Service::onResponseSent(const boost::system::error_code &ec,
                    std::size_t bytes_transferred) {
    if (ec.value() != 0) {
        BOOST_LOG(logger::Error()) << "Error on response sent. Code = "
                    << ec.value()
                    << ". Message: " << ec.message();

        onFinish();
    } else {

        onFinish(false);
    }

}


std::string Service::ProcessRequest(asio::streambuf &request) {

    size_t n = 0;
    std::string requestData;
    std::istream strm(&request);
    std::getline(strm, requestData);
    BOOST_LOG(logger::Normal()) << "Request: " << requestData;

    auto result = _container->store(std::stoull(requestData));
    std::string response = "Response from server ";
    response += std::to_string(result);
    response += "\n";

    BOOST_LOG(logger::Normal()) << response;    

    return response;
}