#include <boost/asio.hpp>

#include <thread>
#include <atomic>
#include <memory>

#include "Server.hpp"
#include "Common.hpp"
#include "UI.hpp"
#include "Logger.hpp"


int main() {
    logger::initLogger("server_log.txt");
    
    auto dumpsDir = std::filesystem::current_path();
    dumpsDir.append("dumps");
    Server srv(dumpsDir);

    srv.start(common::portNumber(), common::threadsCount());

    common::UI ui;
    ui.wait("Server running. Close window to stop");

    srv.stop();
}