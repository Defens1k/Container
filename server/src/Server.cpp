#include "Server.hpp"
#include "Common.hpp"
#include "Logger.hpp"

using namespace boost;

Server::Server(std::filesystem::path dumpsDir) {
    _work.reset(new boost::asio::io_service::work(_ios));
    auto dumper = std::make_shared<BinaryFileDumper>(dumpsDir);
    _container = std::make_shared<Container>(std::move(dumper), std::chrono::seconds(common::N));
}

void Server::start(unsigned short portNum,
            unsigned int threadPoolSize) {

    assert(threadPoolSize > 0);

    _acc.reset(new Acceptor(_ios, portNum, _container));
    _acc->Start();

    for (unsigned int i = 0; i < threadPoolSize; i++) {
        std::unique_ptr<std::thread> th(
            new std::thread([this]()
                            { _ios.run(); }));

        _threadPool.push_back(std::move(th));
    }
}

void Server::stop() {
    _acc->Stop();
    _ios.stop();

    for (auto &th : _threadPool) {
        th->join();
    }
}