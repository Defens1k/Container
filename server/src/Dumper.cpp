#include "Dumper.hpp"
#include "Logger.hpp"
#include <fstream>
#include <boost/locale.hpp>
namespace {
using char_type = std::filesystem::path::value_type;
} //namespcae 



BinaryFileDumper::BinaryFileDumper(std::filesystem::path dumpsDir) : _dumpsDir(dumpsDir) {
    if (std::filesystem::exists(_dumpsDir) && !std::filesystem::is_directory(_dumpsDir)) {
        BOOST_LOG(logger::Error()) << "Not a directory: " << _dumpsDir.native();
        throw std::runtime_error(std::string("Not a directory") + _dumpsDir.string());
    }
    std::filesystem::create_directory(_dumpsDir);
}

std::filesystem::path::string_type BinaryFileDumper::dumpName() {
    auto t = std::time(nullptr);
    auto tm = std::localtime(&t);
    std::basic_stringstream<char_type> stream;
    auto fmt = boost::locale::conv::utf_to_utf<char_type>("%d_%m_%y___%H_%M_%S");
    stream << "Dump" << _dumpNumber << "_" << std::put_time<char_type>(tm, fmt.c_str()) << ".dump";
    _dumpNumber++;
    return std::move(stream.str());
    //return L"file.txt";
}

void BinaryFileDumper::dump(const std::vector<size_t> & data) {
    std::filesystem::current_path(_dumpsDir);

    auto filename = dumpName();
    auto filePath = std::filesystem::current_path();
    filePath.append(filename);

    std::basic_ofstream<char_type> file;
    file.open(filename.c_str(), std::ios_base::binary);
    if (!file.is_open()) {
        BOOST_LOG(logger::Error()) << "Can not open file: " << filePath.native();
        return;
    }



    const char_type* binaryData = reinterpret_cast<const char_type*>(data.data());
    size_t binarySize = data.size() * (sizeof(size_t) / sizeof(char_type));

    BOOST_LOG(logger::Normal()) << "Starting write dump in file: " << filePath.native();
    file.write(binaryData, binarySize);
} 
